using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using library10.Models;
using library10.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace library10.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class ApplicationRoleController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly ApplicationDbContext _context;

        public ApplicationRoleController(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var alluserRole = _context.UserRoles.ToList();
            List<ApplicationRoleViewModel> model = new List<ApplicationRoleViewModel>();
            model = _roleManager.Roles.Select(r => new ApplicationRoleViewModel
            {                
                NumberOfUsers = alluserRole.Count(ur=>ur.RoleId==r.Id),
                Id = r.Id,
                Name = r.Name,
                Description = r.Description
            }).ToList();


            return View(model);

        }

        [HttpGet]
        public async Task<IActionResult> AddEditRole(string id)
        {
            ApplicationRoleViewModel model = new ApplicationRoleViewModel();
            if (!string.IsNullOrEmpty(id)) //baraye virayesh
            {
                // peyda kardan naqsh
                ApplicationRole applicationRole = await _roleManager.FindByIdAsync(id);

                if (applicationRole != null) // agar id peydashod
                {
                    model.Id = applicationRole.Id;
                    model.Name = applicationRole.Name;
                    model.Description = applicationRole.Description;

                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            return PartialView("_AddEditApplicationRole", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddEditRole(string id, ApplicationRoleViewModel model, string redirecturl)
        {
            if (ModelState.IsValid)
            {
                bool isExist = !String.IsNullOrEmpty(id);
                ApplicationRole applicationrole = isExist
                    ? await _roleManager.FindByIdAsync(id)
                    : new ApplicationRole
                    {

                    };
                applicationrole.Name = model.Name;
                applicationrole.Description = model.Description;

                IdentityResult roleResult = isExist ? await _roleManager.UpdateAsync(applicationrole) :
               await _roleManager.CreateAsync(applicationrole);
                if (roleResult.Succeeded)
                {
                    return PartialView("_SuccessFullyResponse", redirecturl);
                }
            }
           
                return PartialView("_AddEditApplicationRole", model);

           
        }
        [HttpGet]
        public async Task<IActionResult> DeleteRole(string id)
        {
            // srring motaghayer
            string name = string.Empty;
            //string class
            if (!String.IsNullOrEmpty(id))
            {
                ApplicationRole ar = await _roleManager.FindByIdAsync(id);
                if (ar!=null)
                {
                    name = ar.Description;
                }
            }
            return PartialView("_DeleteRole", name);
        }
        [HttpPost]
        public async Task<IActionResult> DeleteRole(string id, IFormCollection form)
        {
            if (!String.IsNullOrEmpty(id))
            {
                ApplicationRole ar = await _roleManager.FindByIdAsync(id);
                if (ar!=null)
                {
                    IdentityResult roleResult =  _roleManager.DeleteAsync(ar).Result;
                    if (roleResult.Succeeded)
                    {
                        return RedirectToAction("Index");
                    }
                }
            }
            return View();
        }

    }
}


