using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using library10.Models;
using library10.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ReflectionIT.Mvc.Paging;

namespace library10.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]

    public class AuthorController : Controller
    {
      //  private readonly ApplicationDbContext _context;
       // private readonly IServiceProvider _iServiceProvider;
        private readonly IAuthorService _ias;

        public AuthorController(IAuthorService ias)
        {
           // _context = context;
           // _iServiceProvider = iServiceProvider;
            _ias = ias;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {

            var model = await _ias.AuthorListAsync();

            return View(model);


            // or


            //List<Author> model = new List<Author>();

            //model = _context.Authors.Select(a => new Author
            //    {
            //       AuthorId = a.AuthorId,
            //       AuthorName = a.AuthorName,
            //       AuthorDiscription = a.AuthorDiscription

            //    }

            //).ToList();

            //return View(model);
        }

        public async Task<IActionResult> SearchAuthor(string authorSearch)
        {
            var modelpaging = await _ias.SearchAuthorAsync(authorSearch);
            return View("Index", modelpaging);

        }
        [HttpGet]
        public async Task<IActionResult> AddEditAuthor(int Id)
        {
            var author = new Author();

            if (Id != 0)
            {
                author = await _ias.GetAuthorByIdAsync(Id);
                if (author==null)
                {
                    return RedirectToAction("Index");
                }
                //using (var db= _iServiceProvider.GetRequiredService<ApplicationDbContext>())
                //{
                //    author = _context.Authors.Where(a => a.AuthorId == Id).SingleOrDefault();
                //    if (author==null)
                //    {
                //        return RedirectToAction("Index");
                //    }
                //}
            }
            return PartialView("_AddEditAuthor",author);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddEditAuthor(Author model, int id,string redirecturl)
        {
            if (ModelState.IsValid)
            {
                if (id==0)
                {
                    _ias.InsertAuthor(model);
                    _ias.Save();
                   // using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>() )
                 //   {
                        //db.Authors.Add(model);
                       // db.SaveChanges();
                  //  }
                    return PartialView("_SuccessFullyResponse",redirecturl);
                }
                else
                {
                    _ias.EditAuthor(model);
                    _ias.Save();
                    //using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>() )
                   // {
                      //  db.Authors.Update(model);
                      //  db.SaveChanges();

                   // }
                    return PartialView("_SuccessFullyResponse",redirecturl);
                }
            }
            else
            {
                return PartialView("_AddEditAuthor", model);
            }
        }
        [HttpGet]
        public async Task<IActionResult> DeleteAuthor(int id)
        {
            var tlbAuthor = new Author();
            tlbAuthor = await _ias.GetAuthorByIdAsync(id);
            //using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
           // {
                //پیدا کردن رکورد مطابق با ای 
              //  tlbAuthor = db.Authors.Where(a => a.AuthorId == id).SingleOrDefault();

                if (tlbAuthor == null)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return PartialView("_DeleteAuthor", tlbAuthor.AuthorName);
                }
           // }
        }
        [HttpPost]
        public async Task<IActionResult> DeleteAuthor(int id, string d)
        {
            var tlbAuthor = await _ias.GetAuthorByIdAsync(id);
            _ias.DeleteAuthor(tlbAuthor);
            _ias.Save();
            return RedirectToAction("Index");

            //using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
           // {
               // var tlbauthor = db.Authors.Where(a => a.AuthorId == id).SingleOrDefault();


               // db.Authors.Remove(tlbauthor);
               // db.SaveChanges();
               

           // }
        }
    }
}