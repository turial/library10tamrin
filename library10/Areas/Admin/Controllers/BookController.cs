using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using library10.Models;
using library10.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Linq;
using ReflectionIT.Mvc.Paging;

namespace library10.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]

    public class BookController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IServiceProvider _iServiceProvider;
        private readonly UserManager<ApplicationUser> _userManager;

        private readonly IHostingEnvironment _appEnvironment;

        public BookController(ApplicationDbContext context, IServiceProvider iServiceProvider, UserManager<ApplicationUser> userManager, IHostingEnvironment appEnvironment)
        {
            _context = context;
            _iServiceProvider = iServiceProvider;
            _userManager = userManager;
            _appEnvironment = appEnvironment;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int page = 1)
        {
            //List<BookListViewModel> model = new List<BookListViewModel>();
            //linq to query string

            var query = (from b in _context.Books

                         join a in _context.Authors on b.AuthorID equals a.AuthorId
                         join bg in _context.BookGroups on b.BookgroupID equals bg.BookGroupId

                         select new BookListViewModel()
                         {
                             BookId = b.BookId,
                             BookName = b.BookName,
                             BookPageCount = b.BookPageCount,
                             BookImage = b.BookImage,
                             AuthorId = b.AuthorID,
                             BookGroupId = b.BookgroupID,
                             AuthorName = a.AuthorName,
                             BookGroupName = bg.BookGroupName,
                             Price = b.Price
                         }).AsNoTracking().OrderBy(b => b.BookId);
            //foreach (var item in query)
            //{
            //    BookListViewModel objmodel = new BookListViewModel();

            //    objmodel.BookId = item.BookId;
            //    objmodel.BookName = item.BookName;
            //    objmodel.BookImage = item.BookImage;
            //    objmodel.BookPageCount = item.BookPageCount;
            //    objmodel.AuthorId = item.AuthorID;
            //    objmodel.BookGroupId = item.BookgroupID;
            //    objmodel.AuthorName = item.AuthorName;
            //    objmodel.BookGroupName = item.BookGroupName;

            //    model.Add(objmodel);

            //}
            var modelpaging = await PagingList<BookListViewModel>.CreateAsync(query, 10, page);
            ViewBag.Rootpath = "/upload/thumbnailImage/";
            return View(modelpaging);
        }

        public async Task<IActionResult> SearchBook(string bookSearch, string authorSearch, string bookgroupSearch, int page = 1)
        {

            var query = (from b in _context.Books

                         join a in _context.Authors on b.AuthorID equals a.AuthorId
                         join bg in _context.BookGroups on b.BookgroupID equals bg.BookGroupId

                         select new BookListViewModel()
                         {
                             BookId = b.BookId,
                             BookName = b.BookName,
                             BookPageCount = b.BookPageCount,
                             BookImage = b.BookImage,
                             AuthorId = b.AuthorID,
                             BookGroupId = b.BookgroupID,
                             AuthorName = a.AuthorName,
                             Price = b.Price,
                             BookGroupName = bg.BookGroupName
                         }).AsNoTracking().OrderBy(b => b.BookId);
            //foreach (var item in query)
            //{
            //    BookListViewModel objmodel = new BookListViewModel();
            //    objmodel.BookId = item.BookId;
            //    objmodel.BookName = item.BookName;
            //    objmodel.BookImage = item.BookImage;
            //    objmodel.BookPageCount = item.BookPageCount;
            //    objmodel.AuthorId = item.AuthorID;
            //    objmodel.BookGroupId = item.BookgroupID;
            //    objmodel.AuthorName = item.AuthorName;
            //    objmodel.BookGroupName = item.BookGroupName;
            //    model.Add(objmodel);
            //}
            var modelpaging = await PagingList<BookListViewModel>.CreateAsync(query, 10, page);

            if (bookSearch != null)
            {
                modelpaging = await PagingList<BookListViewModel>.CreateAsync(
                    query.Where(b => b.BookName.Contains(bookSearch)).OrderBy(b => b.BookId), 10, page);
            }
            if (bookgroupSearch != null)
            {
                modelpaging = await PagingList<BookListViewModel>.CreateAsync(
                    query.Where(bg => bg.BookGroupName.Contains(bookgroupSearch)).OrderBy(bg => bg.BookId), 10, page);
            }
            if (authorSearch != null)
            {
                modelpaging = await PagingList<BookListViewModel>.CreateAsync(
                    query.Where(a => a.AuthorName.Contains(authorSearch)).OrderBy(b => b.BookId), 10, page);
            }



            ViewBag.Rootpath = "/upload/thumbnailImage/";
            return View("Index", modelpaging);

        }
        [HttpGet]
        public IActionResult AddBook()
        {
            /// meqdardehi be drop down list ha


            AddEditBookViewModel model = new AddEditBookViewModel();

            model.BookGroups = _context.BookGroups.Select(bg => new SelectListItem
            {
                Text = bg.BookGroupName,
                // BookGroupId az noe int gereftitm va value az noe stringe braye hamin az tostring() estefade kardim
                Value = bg.BookGroupId.ToString()
            }).ToList();
            model.Authors = _context.Authors.Select(a => new SelectListItem
            {
                Text = a.AuthorName,
                Value = a.AuthorId.ToString()


            }).ToList();
            return PartialView("_AddEditBook", model);
        }

        [HttpGet]
        public IActionResult EditBook(int id)
        {
            AddEditBookViewModel model = new AddEditBookViewModel();

            model.BookGroups = _context.BookGroups.Select(bg => new SelectListItem
            {
                Text = bg.BookGroupName,
                // BookGroupId az noe int gereftitm va value az noe stringe braye hamin az tostring() estefade kardim
                Value = bg.BookGroupId.ToString()
            }).ToList();
            model.Authors = _context.Authors.Select(a => new SelectListItem
            {
                Text = a.AuthorName,
                Value = a.AuthorId.ToString()


            }).ToList();

            if (id != 0)
            {
                using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
                {
                    Book book = _context.Books.Where(b => b.BookId == id).SingleOrDefault();
                    if (book != null)
                    {
                        model.BookId = book.BookId;
                        model.BookName = book.BookName;
                        model.BookDescription = book.BookDescription;
                        model.BookPageCount = book.BookPageCount;
                        model.AuthorId = book.AuthorID;
                        model.BookGroupId = book.BookgroupID;
                        model.Price = book.Price;
                        model.BookImage = book.BookImage;
                    }
                }
            }

            return PartialView("_AddEditBook", model);
        }

        [HttpPost]
        // [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddEditBook(AddEditBookViewModel model, int bookId,
            IEnumerable<IFormFile> files, string ImageName)
        {

            if (ModelState.IsValid)
            {
                ///upload iamge//////
                var uploads = Path.Combine(_appEnvironment.WebRootPath, "upload\\normalImage\\");
                foreach (var file in files)
                {
                    if (file != null && file.Length > 0)
                    {
                        var filename = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);

                        using (var filestream = new FileStream(Path.Combine(uploads, filename), FileMode.Create))
                        {
                            await file.CopyToAsync(filestream);
                            model.BookImage = filename;
                        }

                        ///ایجاد تصاویر بند انگشتی یا تصاویر کوچک ///ایجاد تصاویر بند انگشتی یا تصاویر کوچک
                        InsertShowImage.ImageResizer img = new InsertShowImage.ImageResizer();
                        img.Resize(uploads + filename,
                            _appEnvironment.WebRootPath + "\\upload\\thumbnailImage\\" + filename);
                    }
                }
                ///upload image///

                if (bookId == 0)
                {
                    if (model.BookImage == null)
                    {
                        // اگر تصویر انتخاب نشده بود.
                        model.BookImage = "defaultpic.png";
                    }

                    //afzodan
                    using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
                    {
                        Book bookmodel = AutoMapper.Mapper.Map<AddEditBookViewModel, Book>(model);

                        db.Books.Add(bookmodel);
                        db.SaveChanges();
                    }

                    return Json(new { status = "success", message = "اطلاعات کتاب با موفقیت ایجاد شد." });
                }
                else
                {
                    // virayesh
                    using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
                    {
                        if (model.BookImage == null)
                        {
                            //در حالت ویرایش اگر تصویر خالی بود با ای میج نیم که با استرینگ ارسال شده بود را جایگزین میکنیم
                            model.BookImage = ImageName;
                        }

                        Book bookmodel = AutoMapper.Mapper.Map<AddEditBookViewModel, Book>(model);

                        db.Books.Update(bookmodel);
                        db.SaveChanges();
                    }

                    return Json(new { status = "success", message = "اطلاعات کتاب با موفقیت ویرایش شد." });

                }
            }

            model.BookGroups = _context.BookGroups.Select(bg => new SelectListItem
            {
                Text = bg.BookGroupName,
                // BookGroupId az noe int gereftitm va value az noe stringe braye hamin az tostring() estefade kardim
                Value = bg.BookGroupId.ToString()
            }).ToList();
            model.Authors = _context.Authors.Select(a => new SelectListItem
            {
                Text = a.AuthorName,
                Value = a.AuthorId.ToString()


            }).ToList();


            //نشان دادن ولیدیشن ها با جی کوئری ایجکس

            var list = new List<string>();

            foreach (var validation in ViewData.ModelState.Values)
            {
                list.AddRange(validation.Errors.Select(error => error.ErrorMessage));
            }

            return Json(new { status = "error", error = list });



            //if (ModelState.IsValid)
            //{
            //    if (BookId==0)
            //    {//afzodan
            //        using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
            //        {
            //            Book bookmodel = AutoMapper.Mapper.Map<AddEditBookViewModel, Book>(model);

            //            db.Books.Add(bookmodel);
            //            db.SaveChanges();
            //        }
            //        return PartialView("_SuccessFullyResponse", redirecturl);
            //    }
            //    else
            //    {
            //        //virayesh
            //        using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
            //        {
            //            Book bookmodel = AutoMapper.Mapper.Map<AddEditBookViewModel, Book>(model);

            //            db.Books.Update(bookmodel);
            //            db.SaveChanges();
            //        }
            //        return PartialView("_SuccessFullyResponse", redirecturl);
            //    }
            //}
            //model.BookGroups = _context.BookGroups.Select(bg => new SelectListItem
            //{
            //    Text = bg.BookGroupName,
            //    // BookGroupId az noe int gereftitm va value az noe stringe braye hamin az tostring() estefade kardim
            //    Value = bg.BookGroupId.ToString()
            //}).ToList();
            //model.Authors = _context.Authors.Select(a => new SelectListItem
            //{
            //    Text = a.AuthorName,
            //    Value = a.AuthorId.ToString()


            //}).ToList();
            //return PartialView("_AddEditBook", model);
        }

        [HttpGet]
        public async Task<IActionResult> DeleteBook(int id)
        {
            var model = new Book();
            using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
            {
                model = db.Books.Where(b => b.BookId == id).SingleOrDefault();
                if (model == null)
                {
                    return RedirectToAction("Index");
                }

            }

            return PartialView("_DeleteBook", model.BookName);

        }

        [HttpPost]
        public async Task<IActionResult> DeleteBook(int id, IFormCollection form)
        {
            using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
            {
                var model = _context.Books.Where(b => b.BookId == id).SingleOrDefault();

                //چون بعد از حذف عکس ها در پوشه ها داخل نرم افزار میماند میتوانیم به صورت دستی در محل ذخیره پروژه عکس ها را حف کنیم وبرنامه را باز و بسته کنیم 
                //برای حذف عکس از روت سایت به صورت خودکار
                if (model.BookImage != "defaultpic.png")
                {
                    //برای جلو گیری از حذف تصویر پیش فرض درصورت نداشتن عکس


                    ////////////normalimage/////////////
                    var pathnormal = Path.Combine(_appEnvironment.WebRootPath, "upload\\normalImage\\") +
                                     model.BookImage;
                    if (System.IO.File.Exists(pathnormal))
                    {
                        System.IO.File.Delete(pathnormal);
                    }
                    ////////////////thumbnailimage////////////

                    var paththumbnail = Path.Combine(_appEnvironment.WebRootPath, "upload\\thumbnailImage\\") +
                                        model.BookImage;
                    if (System.IO.File.Exists(paththumbnail))
                    {
                        System.IO.File.Delete(paththumbnail);
                    }
                }

                db.Books.Remove(model);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult BookDetails(int id)
        {
            if (id == 0)
            {
                return RedirectToAction("NotFounds");
            }
            var model = new MultiModels();
            model.Userlisted = (from u in _userManager.Users orderby u.Id descending select u).Take(10).ToList();
            model.LastNews = (from n in _context.news orderby n.NewsId descending select n).Take(5).ToList();
            model.bookDetail = (from b in _context.Books
                                join a in _context.Authors on b.AuthorID equals a.AuthorId
                                join bg in _context.BookGroups on b.BookgroupID equals bg.BookGroupId

                                where b.BookId == id
                                select new BookDetailsViewModel
                                {
                                    BookId = b.BookId,
                                    BookDescription = b.BookDescription,
                                    BookPageCount = b.BookPageCount,
                                    BookImage = b.BookImage,
                                    BookName = b.BookName,
                                    AuthorName = a.AuthorName,
                                    BookGroupName = bg.BookGroupName,
                                    BookStock = b.BookStock,
                                    BookViews = b.BookViews,
                                    Price = b.Price,
                                    BookLikeCount = b.BookLikeCount
                                }).ToList();
            ///////////////////////////////////////
            ///دستورات مربوط به بازدید.

            using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
            {
                var result = (from b in _context.Books where b.BookId == id select b);
                var currentbook = result.FirstOrDefault();
                if (result.Count() != 0)
                {
                    currentbook.BookViews++;
                    db.Books.Attach(currentbook);
                    db.Entry(currentbook).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    db.SaveChanges();

                }
            }

            ViewBag.imagepath = "/upload/normalImage/";
            return View(model);
        }
        public IActionResult NotFounds()
        {
            return View("NotFounds");
        }

        [AllowAnonymous]
        public async Task<IActionResult> Like(int bId)
        {
            using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
            {
                //check book for exist
                var quary = db.Books.Where(b => b.BookId == bId).SingleOrDefault();

                if (quary == null)
                {
                    //return RedirectToAction("Index"); 
                    return Redirect(Request.Headers["Referer"].ToString());
                }

                //چک کردن این که ایا کوکی از قبل به وجود امده است یا خیر

                if (Request.Cookies["Like"] == null)
                {
                    Response.Cookies.Append("Like", "," + bId + ",", new Microsoft.AspNetCore.Http.CookieOptions() { Expires = DateTime.Now.AddYears(5) });

                    quary.BookLikeCount++;

                    db.Update(quary);
                    await db.SaveChangesAsync();

                    //return Redirect(Request.Headers["Referer"].ToString());
                    return Json(new { status = "success", message = "رای شما ثبت شد.", result = quary.BookLikeCount });
                }
                else
                {
                    //اگر کوکی از قبل وجود داشت

                    string cookiecontent = Request.Cookies["Like"].ToString();

                    if (cookiecontent.Contains("," + bId + ","))
                    {
                        //یعنی کار بر قبلا رای داده است
                        return Redirect(Request.Headers["Referer"].ToString());
                    }

                    else
                    {
                        cookiecontent += "," + bId + ",";

                        Response.Cookies.Append("Like", cookiecontent, new Microsoft.AspNetCore.Http.CookieOptions() { Expires = DateTime.Now.AddYears(5) });

                        quary.BookLikeCount++;

                        db.Update(quary);
                        await db.SaveChangesAsync();

                        return Json(new { status = "success", message = "رای شما ثبت شد.", result = quary.BookLikeCount });
                    }


                }

            }

        }


        [AllowAnonymous]
        public async Task<IActionResult> DisLike(int bId)
        {
            using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
            {
                //check book for exist
                var quary = db.Books.Where(b => b.BookId == bId).SingleOrDefault();

                if (quary == null)
                {
                    //return RedirectToAction("Index"); 
                    return Redirect(Request.Headers["Referer"].ToString());
                }

                //چک کردن این که ایا کوکی از قبل به وجود امده است یا خیر

                if (Request.Cookies["disLike"] == null)
                {
                    Response.Cookies.Append("disLike", "," + bId + ",", new Microsoft.AspNetCore.Http.CookieOptions() { Expires = DateTime.Now.AddYears(5) });

                    quary.BookLikeCount--;

                    db.Update(quary);
                    await db.SaveChangesAsync();

                    return Json(new { status = "success", message = "رای شما ثبت شد.", result = quary.BookLikeCount });
                }
                else
                {
                    //اگر کوکی از قبل وجود داشت

                    string cookiecontent = Request.Cookies["disLike"].ToString();

                    if (cookiecontent.Contains("," + bId + ","))
                    {
                        //یعنی کار بر قبلا رای داده است
                        return Redirect(Request.Headers["Referer"].ToString());
                    }

                    else
                    {
                        cookiecontent += "," + bId + ",";

                        Response.Cookies.Append("disLike", cookiecontent, new Microsoft.AspNetCore.Http.CookieOptions() { Expires = DateTime.Now.AddYears(5) });

                        quary.BookLikeCount--;

                        db.Update(quary);
                        await db.SaveChangesAsync();

                        return Json(new { status = "success", message = "رای شما ثبت شد.", result = quary.BookLikeCount });
                    }


                }

            }

        }


        [AllowAnonymous]
        [Authorize(Roles = "User")]
        public IActionResult Borrow(int Id)
        {

            using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
            {//کنترل اینکه ای دی ارسال شده وجود دارد یاخیر
                var query = db.Books.Where(b => b.BookId == Id).SingleOrDefault();
                if (query == null)
                {
                    return Json(new { status = "fail", message = "این کتاب در کتاب خانه وجود ندارد." });
                }

                if (query.BookStock == 0)
                {
                    //    اگر کتاب  موجود نبود
                    return Json(new { status = "success", message = "این کتاب موجود نیست." });

                }
                else
                {
                    //اگر کتاب موجودی داشت
                    if (Request.Cookies["_brbook"] == null)
                    {
                        Response.Cookies.Append("_brbook", "," + Id + ",", new Microsoft.AspNetCore.Http.CookieOptions()
                        { Expires = DateTime.Now.AddMinutes(30) });
                        return Json(new { status = "success", message = "کتاب به لیست درخواستی شما افزوده شد.", sabadcount = 1 });

                    }
                    else
                    {
                        string cookiecontent = Request.Cookies["_brbook"].ToString();

                        if (cookiecontent.Contains("," + Id + ","))
                        {
                            return Json(new { status = "success", message = "این کتاب از قبل در لیست درخواستی شما موجود است." });
                        }
                        else
                        {
                            cookiecontent += "," + Id + ",";
                            Response.Cookies.Append("_brbook", cookiecontent, new Microsoft.AspNetCore.Http.CookieOptions() { Expires = DateTime.Now.AddMinutes(30) });

                            string[] requestbookcount = cookiecontent.Split(',');

                            requestbookcount = requestbookcount.Where(r => r != "").ToArray();




                            return Json(new
                            {
                                status = "success",
                                message = "کتاب به لیسمت در خواستی اضافه شد.",
                                sabadcount = requestbookcount.Count()
                            });
                        }
                    }
                }

            }


        }
        [AllowAnonymous]
        [Authorize(Roles = "User")]
        public IActionResult RequestedBook()
        {
            var model = new MultiModels();
            model.Userlisted = (from u in _userManager.Users orderby u.Id descending select u).Take(10).ToList();
            model.LastNews = (from n in _context.news orderby n.NewsId descending select n).Take(5).ToList();
            if (Request.Cookies["_brbook"] != null)
            {
                string cookiecontent = Request.Cookies["_brbook"].ToString();
                string[] requestedbook = cookiecontent.Split(',');
                requestedbook = requestedbook.Where(r => r != "").ToArray();
                model.SearchedBooks = (from b in _context.Books
                                       where requestedbook.Contains(b.BookId.ToString())
                                       select new Book
                                       {
                                           BookId = b.BookId,
                                           BookName = b.BookName,
                                           Price = b.Price
                                       }).ToList();

            }
            var querfullname = (from u in _context.Users where u.Id == _userManager.GetUserId(HttpContext.User) select u).SingleOrDefault();
            ViewBag.kifepool = querfullname.KifePool;
            ViewBag.FullName = querfullname.FirstName + " " + querfullname.LastName;

            ViewBag.imagepath = "/upload/normalImage/";
            return View(model);
        }
        [AllowAnonymous]
        [Authorize(Roles = "User")]
        public IActionResult DeleteRequestedBook(int Id)
        {
            string CookieContent = Request.Cookies["_brbook"].ToString();
            string[] bookIdRequested = CookieContent.Split(',');
            bookIdRequested = bookIdRequested.Where(b => b != "").ToArray();
            //اضافه کردن یک ارایه به لیست
            List<string> idList = new List<string>(bookIdRequested);
            idList.Remove(Id.ToString());
            //کوکی کانتنت را خالی میکنیم وبعد لیست را داخلش میریزم
            CookieContent = "";
            for (int i = 0; i < idList.Count; i++)
            {
                CookieContent += "," + idList[i] + ",";
            }
            //حالا باید مقدار کوکی کانتنت رو بریزنم تو کولی اصلی
            Response.Cookies.Append("_brbook", CookieContent, new Microsoft.AspNetCore.Http.CookieOptions()
            { Expires = DateTime.Now.AddMinutes(30) });
            var model = new MultiModels();
            model.Userlisted = (from u in _userManager.Users orderby u.Id descending select u).Take(10).ToList();
            model.LastNews = (from n in _context.news orderby n.NewsId descending select n).Take(5).ToList();
            if (Request.Cookies["_brbook"] != null)
            {
                string[] requestedbook = CookieContent.Split(',');
                requestedbook = requestedbook.Where(r => r != "").ToArray();
                model.SearchedBooks = (from b in _context.Books
                                       where requestedbook.Contains(b.BookId.ToString())
                                       select new Book
                                       {
                                           BookId = b.BookId,
                                           BookName = b.BookName,
                                           Price = b.Price

                                       }).ToList();


            }
            var querfullname = (from u in _context.Users where u.Id == _userManager.GetUserId(HttpContext.User) select u).SingleOrDefault();
            ViewBag.kifepool = querfullname.KifePool;
            ViewBag.FullName = querfullname.FirstName + " " + querfullname.LastName;

            ViewBag.imagepath = "/upload/normalImage/";
            return View("RequestedBook", model);

        }
        [AllowAnonymous]
        [Authorize(Roles = "User")]
        public IActionResult BookRequest(string userId, string tp)
        {
            string CookieContent = Request.Cookies["_brbook"].ToString();
            string[] bookIdRequested = CookieContent.Split(',');
            bookIdRequested = bookIdRequested.Where(b => b != "").ToArray();
            ///کنترل اینکه کار بر مجدد کتاب تکرای را ثبت نکند

            if (Request.Cookies["_brbook"] != null)
            {
                string[] requestedbook = CookieContent.Split(',');
                requestedbook = requestedbook.Where(r => r != "").ToArray();
                var query = (from b in _context.BorrowBooks
                             where requestedbook.Contains(b.BookId.ToString()) && b.UserId == userId && b.Flag == 1
                             select b
                    ).ToList();
                if (query.Count > 0)
                {
                    return Json(new { status = "success", message = "لیست درخواستی شما شامل کتاب هایی می باشد که قبلا درخواست داده اید. " });

                }
            }
            
            ///ثبت دیتابیس
            using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        var currentDay = DateTime.Now;
                        PersianCalendar pcalender = new PersianCalendar();
                        int year = pcalender.GetYear(currentDay);
                        int month = pcalender.GetMonth(currentDay);
                        int day = pcalender.GetDayOfMonth(currentDay);
                        string ShamsiDate = string.Format("{0:yyyy/MM/dd}",
                            Convert.ToDateTime(year + "/" + month + "/" + day));

                        var kifepullQuery = (from U in db.Users where U.Id == _userManager.GetUserId(User) select U)
                            .SingleOrDefault();
                        if (kifepullQuery.KifePool < Convert.ToInt32(tp))
                        {
                            return Json(new { status = "fail", message = "موجودی کافی نمی باشد." });
                        }
                        else
                        {
                            for (int i = 0; i < bookIdRequested.Count(); i++)
                            {
                                BorrowBook br = new BorrowBook();
                                br.BookId = Convert.ToInt32(bookIdRequested[i]);
                                br.UserId = userId;
                                //وقتی کاربر کتاب را درخواست داد فلگ را یک میکنیم
                                br.Flag = 1;
                                var bookprice = (from b in db.Books
                                                 where b.BookId == Convert.ToInt32(bookIdRequested[i])
                                                 select b);
                                var result1 = bookprice.SingleOrDefault();
                                br.Price = result1.Price;
                                br.RequestDate = ShamsiDate;
                                db.BorrowBooks.Add(br);

                            }

                            kifepullQuery.KifePool = kifepullQuery.KifePool - Convert.ToInt32(tp);
                            db.SaveChanges();
                            transaction.Commit();
                        }

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }

                Response.Cookies.Delete("_brbook");
                return Json(new { status = "success", message = "کتاب های درخواستی شما ثبت شد" });
            }
        }

    }
}