using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using library10.Models;
using library10.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ReflectionIT.Mvc.Paging;

namespace library10.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class BookGroupController : Controller
    {
       // private readonly ApplicationDbContext _context;
       // private readonly IServiceProvider _iServiceProvider;
        private readonly IBookGroupService _IBG;

        public BookGroupController(/*ApplicationDbContext context,*/ /*IServiceProvider iServiceProvider, */IBookGroupService IBG)
        {
            //_context = context;
            //_iServiceProvider = iServiceProvider;
            _IBG = IBG;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var modelpaging = await _IBG.PagingListBookGroupAsync();
            return View(modelpaging);
            //var model = _context.BookGroups.AsNoTracking().Include(b => b.Books).OrderBy(bg=>bg.BookGroupId);
            //var modelpaging = await PagingList<BookGroup>.CreateAsync(model, 10, page);
            //return View(modelpaging);

            /// or

            //List<BookGroup> model = new List<BookGroup>();

            //model = _context.BookGroups.Select(bg => new BookGroup
            //{
            //    BookGroupId = bg.BookGroupId,
            //    BookGroupName = bg.BookGroupName,
            //    BookGroupDescription = bg.BookGroupDescription

            //}).ToList();

            //return View(model);
        }

        public async Task<IActionResult> SearchBookGroup(string groupSearch)
        {
            var modelpaging = await _IBG.SearchBookGroupAsync(groupSearch);
            return View("Index", modelpaging);
        }

        [HttpGet]
        public async Task<IActionResult> AddEditBookGroup(int Id)
        {
            var bookGroup = new BookGroup();

            if (Id != 0)
            {
                bookGroup = await _IBG.GetBookGroupByIdAsync(Id);
                if (bookGroup == null)
                {
                    return RedirectToAction("Index");
                }
                //using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
                //{
                //    bookgroup = _context.BookGroups.Where(a => a.BookGroupId == Id).SingleOrDefault();
                //    if (bookgroup == null)
                //    {
                //        return RedirectToAction("Index");
                //    }
                //}
            }
            return PartialView("_AddEditBookGroup", bookGroup);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddEditBookGroup(BookGroup model, int id, string redirecturl)
        {
            if (ModelState.IsValid)
            {
                // agar fild haa por shode bood

                if (id == 0)
                {
                    _IBG.InsertBookGroup(model);
                    _IBG.Save();
                    // afzodan
                    //  using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
                    // {
                    // db.BookGroups.Add(model);
                    //db.SaveChanges();
                    //   }
                    //return RedirectToAction("Index");
                    return PartialView("_SuccessFullyResponse", redirecturl);
                }
                else
                {
             
              
                    // virayesh ya update
                    
                    _IBG.EditBookGroup(model);
                    _IBG.Save();
                    //using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
                    //{
                    // db.BookGroups.Update(model);
                    //  db.SaveChanges();

                    //}

                    //  return RedirectToAction("Index");
                    return PartialView("_SuccessFullyResponse", redirecturl);

                }
            }
            else
            {
                return PartialView("_AddEditBookGroup", model);
            }

        }
        [HttpGet]
        public async Task<IActionResult> DeleteBookGroup(int id)
        {
            var tblbookgroup = new BookGroup();
            tblbookgroup = await _IBG.GetBookGroupByIdAsync(id);
            //using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
            //{
            //پیدا کردن رکورد مطابق با ای 
           // tblbookgroup = db.BookGroups.Where(bg => bg.BookGroupId == id).SingleOrDefault();
           
            if (tblbookgroup == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return PartialView("_DeleteGroup", tblbookgroup.BookGroupName);
            }
            //  }
        }
        [HttpPost]
        public async Task<IActionResult> DeleteBookGroup(int id, string d)
        {
            var tblbookgroup = await _IBG.GetBookGroupByIdAsync(id);
            if (tblbookgroup==null)
            {
                return RedirectToAction("Index");
            }
            _IBG.DeleteBookGroup(tblbookgroup);
            _IBG.Save();
           // using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
           // {
               // var tblgroup = db.BookGroups.Where(bg => bg.BookGroupId == id).SingleOrDefault();


              //  db.BookGroups.Remove(tblgroup);
               // db.SaveChanges();
                return RedirectToAction("Index");

            //}
        }
    }
}