using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using library10.Models;
using library10.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ReflectionIT.Mvc.Paging;

namespace library10.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class ManageRequestController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IServiceProvider _iServiceProvider;

        public ManageRequestController(ApplicationDbContext context, IServiceProvider iServiceProvider)
        {
            _context = context;
            _iServiceProvider = iServiceProvider;
        }
        public async Task<IActionResult> Index(int page = 1)
        {
            //List<ManageRequestViewModel> model = new List<ManageRequestViewModel>();
            var model = (from br in _context.BorrowBooks
                         join b in _context.Books on br.BookId equals b.BookId
                         join u in _context.Users on br.UserId equals u.Id

                         select new ManageRequestViewModel
                         {
                             Id = br.Id,
                             BookId = br.BookId,
                             UserId = br.UserId,
                             UserFullName = u.FirstName + " " + u.LastName,
                             BookStock = b.BookStock,
                             BookName = b.BookName,
                             Flag = br.Flag,
                             Price = br.Price,
                             RequestDate = br.RequestDate,
                             AnswerDate = br.AnswerDate,
                             BackDate = br.BackDate,
                             FlagDescription = (
                                     br.Flag == 1 ? "درخواست امانت" :
                                     br.Flag == 2 ? "به امانت برده" :
                                     br.Flag == 3 ? "رد درخواست" :
                                     br.Flag == 4 ? "برگردانده" : "نامشخص"
                                     )
                         }).AsNoTracking().OrderBy(r => r.Id);
            var modelpaging = await PagingList<ManageRequestViewModel>.CreateAsync(model, 10, page);
            return View(modelpaging);
        }
        public async Task<IActionResult> SearchInRequests(string fromDate1, string todate1, string userSearch, int page = 1)
        {  //List<ManageRequestViewModel> model = new List<ManageRequestViewModel>();
            var model = (from br in _context.BorrowBooks
                         join b in _context.Books on br.BookId equals b.BookId
                         join u in _context.Users on br.UserId equals u.Id

                         select new ManageRequestViewModel
                         {
                             Id = br.Id,
                             BookId = br.BookId,
                             UserId = br.UserId,
                             UserFullName = u.FirstName + " " + u.LastName,
                             BookStock = b.BookStock,
                             BookName = b.BookName,
                             Flag = br.Flag,
                             Price = br.Price,
                             RequestDate = br.RequestDate,
                             AnswerDate = br.AnswerDate,
                             BackDate = br.BackDate,
                             FlagDescription = (
                                 br.Flag == 1 ? "درخواست امانت" :
                                 br.Flag == 2 ? "به امانت برده" :
                                 br.Flag == 3 ? "رد درخواست" :
                                 br.Flag == 4 ? "برگردانده" : "نامشخص"
                             )
                         }).AsNoTracking().OrderBy(r => r.Id);
            var modelpaging = await PagingList<ManageRequestViewModel>.CreateAsync(model, 10, page);


            if (userSearch != null)
            {
                modelpaging = await PagingList<ManageRequestViewModel>.CreateAsync(
                    model.Where(
                        r => r.UserFullName.Contains(userSearch)).OrderBy(r => r.Id), 10, page);
            }
            if (fromDate1 != null && todate1 == null)
            {
                //ازتاریخ فرام دیت یک به بعد
                modelpaging = await PagingList<ManageRequestViewModel>.CreateAsync(model.Where(
                    t => t.RequestDate.CompareTo(fromDate1) >= 0).OrderBy(t => t.Id), 10, page);
            }

            if (todate1 != null && fromDate1 == null)
            {
                //ازتاریخ تودیت یک به قبل
                modelpaging = await PagingList<ManageRequestViewModel>.CreateAsync(model.Where(
                    t => t.RequestDate.CompareTo(todate1) <= 0).OrderBy(t => t.Id), 10, page);
            }

            if (todate1 != null && fromDate1 != null)
            {
                modelpaging = await PagingList<ManageRequestViewModel>.CreateAsync(model.Where(
                    t => t.RequestDate.CompareTo(todate1) <= 0 && t.RequestDate.CompareTo(todate1) <= 0).OrderBy(t => t.Id), 10, page);

            }
            return View("Index", modelpaging);
        }

        [HttpGet]
        public IActionResult RejectRequest(int id)
        {
            List<ManageRequestViewModel> model = new List<ManageRequestViewModel>();
            model = (from br in _context.BorrowBooks
                     join b in _context.Books on br.BookId equals b.BookId
                     join u in _context.Users on br.UserId equals u.Id
                     where br.Id == id
                     select new ManageRequestViewModel
                     {
                         UserFullName = u.FirstName + " " + u.LastName,
                         BookName = b.BookName
                     }).ToList();
            if (model == null)
            {
                return RedirectToAction("Index");
            }

            ViewBag.PartialType = 1;
            return PartialView("_ManageRequest", model);
        }

        [HttpPost]
        public IActionResult RejectRequest(int id, IFormCollection form)
        {
            using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
            {
                var query = (from br in db.BorrowBooks where br.Id == id select br);
                var result = query.SingleOrDefault();

                var currentDay = DateTime.Now;
                PersianCalendar pcalender = new PersianCalendar();
                int year = pcalender.GetYear(currentDay);
                int month = pcalender.GetMonth(currentDay);
                int day = pcalender.GetDayOfMonth(currentDay);

                string ShamsiDate = string.Format("{0:yyyy/MM/dd}", Convert.ToDateTime(year + "/" + month + "/" + day));
                using (var transaction = db.Database.BeginTransaction())
                {
                    if (query.Count() != 0)
                    {
                        result.Flag = 3;
                        result.AnswerDate = ShamsiDate;
                        db.BorrowBooks.Attach(result).State = Microsoft.EntityFrameworkCore.EntityState.Modified;

                    }

                    var rejectpooll = (from U in db.Users where U.Id == result.UserId select U).SingleOrDefault();
                    rejectpooll.KifePool = rejectpooll.KifePool + result.Price;

                    db.SaveChanges();
                    transaction.Commit();
                }


            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult AcceptRequest(int id)
        {
            List<ManageRequestViewModel> model = new List<ManageRequestViewModel>();
            model = (from br in _context.BorrowBooks
                     join b in _context.Books on br.BookId equals b.BookId
                     join u in _context.Users on br.UserId equals u.Id
                     where br.Id == id
                     select new ManageRequestViewModel
                     {
                         UserFullName = u.FirstName + " " + u.LastName,
                         BookName = b.BookName
                     }).ToList();
            if (model == null)
            {
                return RedirectToAction("Index");
            }

            ViewBag.PartialType = 2;
            return PartialView("_ManageRequest", model);
        }

        [HttpPost]
        public IActionResult AcceptRequest(int id, IFormCollection form)
        {
            using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
            {
                var query = (from br in db.BorrowBooks where br.Id == id select br);
                var result = query.SingleOrDefault();

                var currentDay = DateTime.Now;
                PersianCalendar pcalender = new PersianCalendar();
                int year = pcalender.GetYear(currentDay);
                int month = pcalender.GetMonth(currentDay);
                int day = pcalender.GetDayOfMonth(currentDay);

                string ShamsiDate = string.Format("{0:yyyy/MM/dd}", Convert.ToDateTime(year + "/" + month + "/" + day));
                //////////////////////////////کم کردن موحودی کتاب در صورت پذیرفتن
                var findbookquery = (from b in db.Books where b.BookId == result.BookId select b);

                var resultbook = findbookquery.SingleOrDefault();
                if (findbookquery.Count() != 0)
                {
                    resultbook.BookStock--;
                    db.Books.Attach(resultbook);
                    db.Entry(resultbook).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    db.SaveChanges();
                }

                //////////////////////////////
                if (query.Count() != 0)
                {
                    result.Flag = 2;
                    result.AnswerDate = ShamsiDate;
                    db.BorrowBooks.Attach(result).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    db.SaveChanges();

                }
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult BackBook(int id)
        {
            List<ManageRequestViewModel> model = new List<ManageRequestViewModel>();
            model = (from br in _context.BorrowBooks
                     join b in _context.Books on br.BookId equals b.BookId
                     join u in _context.Users on br.UserId equals u.Id
                     where br.Id == id
                     select new ManageRequestViewModel
                     {
                         UserFullName = u.FirstName + " " + u.LastName,
                         BookName = b.BookName
                     }).ToList();
            if (model == null)
            {
                return RedirectToAction("Index");
            }

            ViewBag.PartialType = 3;
            return PartialView("_ManageRequest", model);
        }
        [HttpPost]
        public IActionResult BackBook(int id, IFormCollection form)
        {
            using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
            {
                var query = (from br in db.BorrowBooks where br.Id == id select br);
                var result = query.SingleOrDefault();

                var currentDay = DateTime.Now;
                PersianCalendar pcalender = new PersianCalendar();
                int year = pcalender.GetYear(currentDay);
                int month = pcalender.GetMonth(currentDay);
                int day = pcalender.GetDayOfMonth(currentDay);

                string ShamsiDate = string.Format("{0:yyyy/MM/dd}", Convert.ToDateTime(year + "/" + month + "/" + day));
                //////////////////////////////کم کردن موحودی کتاب در صورت پذیرفتن
                var findbookquery = (from b in db.Books where b.BookId == result.BookId select b);

                var resultbook = findbookquery.SingleOrDefault();
                if (findbookquery.Count() != 0)
                {
                    resultbook.BookStock++;
                    db.Books.Attach(resultbook);
                    db.Entry(resultbook).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    db.SaveChanges();
                }

                //////////////////////////////
                if (query.Count() != 0)
                {
                    result.Flag = 4;
                    result.BackDate = ShamsiDate;
                    db.BorrowBooks.Attach(result).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    db.SaveChanges();

                }
            }

            return RedirectToAction("Index");
        }

    }
}