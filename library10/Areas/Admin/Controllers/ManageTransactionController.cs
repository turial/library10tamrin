using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using library10.Models;
using library10.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReflectionIT.Mvc.Paging;

namespace library10.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class ManageTransactionController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IServiceProvider _iServiceProvider;

        public ManageTransactionController(ApplicationDbContext context, IServiceProvider iServiceProvider)
        {
            _context = context;
            _iServiceProvider = iServiceProvider;
        }

        public async Task<IActionResult> Index(int page = 1)
        {
            //List<ManageRequestViewModel> model = new List<ManageRequestViewModel>();
            var model = (from pt in _context.PaymentTransactions
                         join u in _context.Users on pt.UserId equals u.Id

                         select new PaymentTransactionViewModel
                         {
                             Id = pt.Id,
                             Amount = pt.Amount,
                             Description = pt.Description,
                             Email = pt.Email,
                             Mobile = pt.Mobile,
                             TransactionDate = pt.TransactionDate,
                             TransactionTime = pt.TransactionTime,
                             TransactionNumber = pt.TransactionNumber,
                             UserFullName = u.FirstName + " " + u.LastName,
                             UserId = pt.UserId
                         }).AsNoTracking().OrderBy(r => r.Id);
            var modelpaging = await PagingList<PaymentTransactionViewModel>.CreateAsync(model, 10, page);
            return View(modelpaging);
        }

        public async Task<IActionResult> SearchInTransactions(string fromDate1, string todate1, string userSearch, int page = 1)
        {
            //List<ManageRequestViewModel> model = new List<ManageRequestViewModel>();
            var model = (from pt in _context.PaymentTransactions
                         join u in _context.Users on pt.UserId equals u.Id

                         select new PaymentTransactionViewModel
                         {
                             Id = pt.Id,
                             Amount = pt.Amount,
                             Description = pt.Description,
                             Email = pt.Email,
                             Mobile = pt.Mobile,
                             TransactionDate = pt.TransactionDate,
                             TransactionTime = pt.TransactionTime,
                             TransactionNumber = pt.TransactionNumber,
                             UserFullName = u.FirstName + " " + u.LastName,
                             UserId = pt.UserId
                         }).AsNoTracking().OrderBy(r => r.Id);
            var modelpaging = await PagingList<PaymentTransactionViewModel>.CreateAsync(model, 10, page);


            if (userSearch != null)
            {
                modelpaging = await PagingList<PaymentTransactionViewModel>.CreateAsync(
                    model.Where(
                        r => r.UserFullName.Contains(userSearch)).OrderBy(r => r.Id), 10, page);
            }
            if (fromDate1 != null && todate1 == null)
            {
                //ازتاریخ فرام دیت یک به بعد
                modelpaging = await PagingList<PaymentTransactionViewModel>.CreateAsync(model.Where(
                    t => t.TransactionDate.CompareTo(fromDate1) >= 0).OrderBy(t => t.Id), 10, page);
            }

            if (todate1 != null && fromDate1 == null)
            {
                //ازتاریخ تودیت یک به قبل
                modelpaging = await PagingList<PaymentTransactionViewModel>.CreateAsync(model.Where(
                    t => t.TransactionDate.CompareTo(todate1) <= 0).OrderBy(t => t.Id), 10, page);
            }

            if (todate1 != null && fromDate1 != null)
            {
                modelpaging = await PagingList<PaymentTransactionViewModel>.CreateAsync(model.Where(
                    t => t.TransactionDate.CompareTo(todate1) <= 0 && t.TransactionDate.CompareTo(todate1) <= 0).OrderBy(t => t.Id), 10, page);

            }
            return View("Index", modelpaging);
        }
    }
}