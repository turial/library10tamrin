using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using library10.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ReflectionIT.Mvc.Paging;

namespace library10.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]

    public class NewsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IServiceProvider _iServiceProvider;
        private readonly IHostingEnvironment _appEnvironment;

        public NewsController(ApplicationDbContext context, IServiceProvider iServiceProvider,
            IHostingEnvironment appEnvironment)
        {
            _context = context;
            _iServiceProvider = iServiceProvider;
            _appEnvironment = appEnvironment;
        }

        public async Task<IActionResult> Index(int page=1)
        {
            //List<News> model = new List<News>();
          var  model = _context.news.AsNoTracking().Select(n => new News
            {
                NewsId = n.NewsId,
                NewsTitle = n.NewsTitle,
                NewsDate = n.NewsDate,
                NewsImage = n.NewsImage
            }).OrderBy(n=>n.NewsId);
          var modelpaging = await PagingList<News>.CreateAsync(model, 10, page);
            ViewBag.Rootpath = "/upload/normalImage/";
            return View(modelpaging);
        }
        public async Task<IActionResult> SearchInNews(string fromDate1, string todate1, string newsSearch,int page=1)
        {
            //List<News> model = new List<News>();
            var model = _context.news.AsNoTracking().Select(n => new News
            {
                NewsId = n.NewsId,
                NewsTitle = n.NewsTitle,
                NewsDate = n.NewsDate,
                NewsImage = n.NewsImage
            }).OrderBy(n => n.NewsId);
            var modelpaging = await PagingList<News>.CreateAsync(model, 10, page);
          
            if (newsSearch!=null)
            {
                modelpaging =
                    await PagingList<News>.CreateAsync(
                        model.Where(n => n.NewsTitle.Contains(newsSearch)).OrderBy(n => n.NewsId), 10, page);
            }
            if (fromDate1 != null && todate1==null)
            {
                //ازتاریخ فرام دیت یک به بعد
                modelpaging =
                    await PagingList<News>.CreateAsync(model.Where(t=>t.NewsDate.CompareTo(fromDate1)>=0).OrderBy(n=>n.NewsId),10,page);
            }

            if (todate1!=null && fromDate1==null)
            {
                //ازتاریخ تودیت یک به قبل
                modelpaging =
                    await PagingList<News>.CreateAsync(model.Where(t => t.NewsDate.CompareTo(todate1) <= 0).OrderBy(n => n.NewsId), 10, page);
            }

            if (todate1 != null && fromDate1 != null)
            {
                modelpaging =
                    await PagingList<News>.CreateAsync(model.Where(t => t.NewsDate.CompareTo(todate1) <= 0 && t.NewsDate.CompareTo(todate1) <= 0).OrderBy(n => n.NewsId), 10, page);

            }
            ViewBag.Rootpath = "/upload/normalImage/";
            return View("Index", modelpaging);
        }
        [HttpGet]
        public IActionResult AddEditNews(int id)
        {
            var model = new News();
            if (id != 0)
            {
                //update
                using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
                {

                    model = _context.news.Where(n => n.NewsId == id).SingleOrDefault();
                    if (model == null)
                    {
                        return RedirectToAction("Index");
                    }
                }
            }
            //بدست اوردن تاریخ شمسی
            var currentDay = DateTime.Now;
            PersianCalendar pcalender = new PersianCalendar();
            int year = pcalender.GetYear(currentDay);
            int month = pcalender.GetMonth(currentDay);
            int day = pcalender.GetDayOfMonth(currentDay);


            string ShamsiDate = string.Format("{0:yyyy/MM/dd}", Convert.ToDateTime(year + "/" + month + "/" + day));

            ViewBag.sdate = ShamsiDate;

            return PartialView("_AddEditNews", model);

        }

        [HttpPost]
        // [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddEditNews(News model, int NewsId, IEnumerable<IFormFile> files,
            string ImageName)
        {

            if (ModelState.IsValid)
            {
                ///upload iamge//////
                var uploads = Path.Combine(_appEnvironment.WebRootPath, "upload\\normalImage\\");
                foreach (var file in files)
                {
                    if (file != null && file.Length > 0)
                    {
                        var filename = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);

                        using (var filestream = new FileStream(Path.Combine(uploads, filename), FileMode.Create))
                        {
                            await file.CopyToAsync(filestream);
                            model.NewsImage = filename;
                        }
                        /////ایجاد تصاویر بند انگشتی یا تصاویر کوچک ///ایجاد تصاویر بند انگشتی یا تصاویر کوچک
                        //InsertShowImage.ImageResizer img = new InsertShowImage.ImageResizer();
                        //img.Resize(uploads + filename, _appEnvironment.WebRootPath + "\\upload\\thumbnailImage\\" + filename);
                    }
                }
                ///upload image///

                if (NewsId == 0)
                {
                    if (model.NewsImage == null)
                    {
                        // اگر تصویر انتخاب نشده بود.
                        model.NewsImage = "notidefault.jpg";
                    }
                    //afzodan
                    using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
                    {
                        // Book bookmodel = AutoMapper.Mapper.Map<AddEditBookViewModel, Book>(model);

                        db.news.Add(model);
                        db.SaveChanges();
                    }
                    return Json(new { status = "success", message = "اطلاعیه با موفقیت ایجاد شد." });
                }
                else
                {
                    // virayesh
                    using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
                    {
                        if (model.NewsImage == null)
                        {
                            //در حالت ویرایش اگر تصویر خالی بود با ای میج نیم که با استرینگ ارسال شده بود را جایگزین میکنیم
                            model.NewsImage = ImageName;
                        }
                        //  Book bookmodel = AutoMapper.Mapper.Map<AddEditBookViewModel, Book>(model);

                        db.news.Update(model);
                        db.SaveChanges();
                    }
                    return Json(new { status = "success", message = "اطلاعیه با موفقیت ویرایش شد." });

                }
            }

            //نشان دادن ولیدیشن ها با جی کوئری ایجکس

            var list = new List<string>();

            foreach (var validation in ViewData.ModelState.Values)
            {
                list.AddRange(validation.Errors.Select(error => error.ErrorMessage));
            }

            return Json(new { status = "error", error = list });

        }




        [HttpGet]
        public async Task<IActionResult> DeleteNews(int id)
        {
            var model = new News();
            using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
            {
                model = db.news.Where(b => b.NewsId == id).SingleOrDefault();
                if (model == null)
                {
                    return RedirectToAction("Index");
                }

            }
            return PartialView("_DeleteNews", model.NewsTitle);

        }

        [HttpPost]
        public async Task<IActionResult> DeleteNews(int id, IFormCollection form)
        {
            using (var db = _iServiceProvider.GetRequiredService<ApplicationDbContext>())
            {
                var model = _context.news.Where(b => b.NewsId == id).SingleOrDefault();

                //چون بعد از حذف عکس ها در پوشه ها داخل نرم افزار میماند میتوانیم به صورت دستی در محل ذخیره پروژه عکس ها را حف کنیم وبرنامه را باز و بسته کنیم 
                //برای حذف عکس از روت سایت به صورت خودکار


                if (model.NewsImage != "notidefault.jpg")
                {
                    //برای جلو گیری از حذف تصویر پیش فرض درصورت نداشتن عکس


                    ////////////normalimage/////////////
                    var pathnormal = Path.Combine(_appEnvironment.WebRootPath, "upload\\normalImage\\") +
                                     model.NewsImage;
                    if (System.IO.File.Exists(pathnormal))
                    {
                        System.IO.File.Delete(pathnormal);
                    }
                    ////////////////thumbnailimage////////////

                    //var paththumbnail = Path.Combine(_appEnvironment.WebRootPath, "upload\\thumbnailImage\\") + model.BookImage;
                    //if (System.IO.File.Exists(paththumbnail))
                    //{
                    //    System.IO.File.Delete(paththumbnail);
                    //} 
                }

                db.news.Remove(model);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult NewsDetails(int id)
        {
            if (id == 0)
            {
                return RedirectToAction("NotFounds");
            }
            var news = _context.news.SingleOrDefault(n => n.NewsId == id);
            if (news == null)
            {
                return RedirectToAction("NotFounds");

            }
            return View(news);
        }

        public IActionResult NotFounds()
        {
            return View("NotFounds");
        }
    }
}