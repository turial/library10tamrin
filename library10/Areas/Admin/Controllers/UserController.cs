using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using library10.Models;
using library10.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ReflectionIT.Mvc.Paging;
using Remotion.Linq.Clauses;

namespace library10.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]

    public class UserController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<ApplicationRole> roleManager;

        public UserController(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int page = 1)
        {
            //نشان دادن اطلاعات به صورت صفحه بندی

            var query = this.userManager.Users.AsNoTracking().Select(

                u => new UserListViewModel
                {
                    Id = u.Id,
                    FullName = u.FirstName + " " + u.LastName,
                    Email = u.Email
                }).OrderBy(u => u.Id);
            var modelpaging = await PagingList<UserListViewModel>.CreateAsync(query, 2, page);
            return View(modelpaging);

            //نشان دادن اطلاعات به صورت عادی
            //    List<UserListViewModel> modelu = new List<UserListViewModel>();

            //    modelu = this.userManager.Users.Select(u => new UserListViewModel
            //    {
            //        Id = u.Id,
            //        FullName = u.FirstName + " " + u.LastName,

            //        Email = u.Email

            //    }

            //    ).ToList();

            //    return View(modelu);
        }

        public async Task<IActionResult> SearchUser(string userSearch,int page=1)
        {
            //نشان دادن اطلاعات به صورت صفحه بندی

            var query = this.userManager.Users.AsNoTracking().Select(

                u => new UserListViewModel
                {
                    Id = u.Id,
                    FullName = u.FirstName + " " + u.LastName,
                    Email = u.Email
                }).OrderBy(u => u.Id);
            var modelpaging = await PagingList<UserListViewModel>.CreateAsync(query, 10, page);

            if (userSearch != null)
            {
                modelpaging =
                    await PagingList<UserListViewModel>.CreateAsync(
                        query.Where(u => u.FullName.Contains(userSearch)).OrderBy(u => u.Id), 2, page);
            }
            return View("Index", modelpaging);
        }







        [HttpGet]
        public IActionResult AddUser()
        {
            UserViewModel model = new UserViewModel();

            model.ApplicationRoles = this.roleManager.Roles.Select(r => new SelectListItem
            {
                Text = r.Name,
                Value = r.Id
            }).ToList();


            return PartialView("_AddUser", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddUser(UserViewModel model, string redirecturl)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = new ApplicationUser
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    PhoneNumber = model.PhoneNumber,
                    UserName = model.UserName,
                    Email = model.Email,
                    Gender = model.Gender

                };
                IdentityResult result = await this.userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    ApplicationRole approle = await this.roleManager.FindByIdAsync(model.ApplicationRoleId);
                    if (approle != null)
                    {
                        IdentityResult roleResult = await this.userManager.AddToRoleAsync(user, approle.Name);
                        if (roleResult.Succeeded)
                        {
                            return PartialView("_SuccessFullyResponse", redirecturl);
                        }
                    }
                }
            }
            model.ApplicationRoles = this.roleManager.Roles.Select(r => new SelectListItem
            {
                Text = r.Name,
                Value = r.Id

            }).ToList();

            return PartialView("_AddUser", model);
        }


        [HttpGet]
        public async Task<IActionResult> EditUser(string id)
        {
            EditUserViewModel model = new EditUserViewModel();

            model.ApplicationRoles = this.roleManager.Roles.Select(r => new SelectListItem
            {
                Text = r.Name,
                Value = r.Id
            }).ToList();

            ////////////////////////////////////////

            if (!String.IsNullOrEmpty(id))
            {
                ApplicationUser User = await userManager.FindByIdAsync(id);
                if (User != null)
                {
                    model.FirstName = User.FirstName;
                    model.LastName = User.LastName;
                    model.Email = User.Email;
                    model.Gender = User.Gender;
                    model.ApplicationRoleId = this.roleManager.Roles.Single(r => r.Name == userManager.GetRolesAsync(User).Result.Single()).Id;
                }
            }
            return PartialView("_EditUser", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditUser(string id, EditUserViewModel model, string redirecturl)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = await this.userManager.FindByIdAsync(id);
                if (user != null)
                {
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.Email = model.Email;
                    user.Gender = model.Gender;

                    string ExistingRole = this.userManager.GetRolesAsync(user).Result.Single();
                    string ExiistingRoleId = this.roleManager.Roles.Single(r => r.Name == ExistingRole).Id;

                    IdentityResult result = await this.userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        if (ExiistingRoleId != model.ApplicationRoleId)
                        {
                            //اگر نقش تغییر کرده بود
                            IdentityResult roleResult = await this.userManager.RemoveFromRoleAsync(user, ExistingRole);
                            if (roleResult.Succeeded)
                            {
                                ApplicationRole approle = await this.roleManager.FindByIdAsync(model.ApplicationRoleId);
                                if (approle != null)
                                {
                                    IdentityResult newRole = await this.userManager.AddToRoleAsync(user, approle.Name);
                                    if (newRole.Succeeded)
                                    {
                                        return PartialView("_SuccessFullyResponse", redirecturl);
                                    }
                                }
                            }
                        }
                        return PartialView("_SuccessFullyResponse", redirecturl);
                    }

                }
            }

            model.ApplicationRoles = roleManager.Roles.Select(r => new SelectListItem
            {
                Text = r.Name,
                Value = r.Id
            }).ToList();
            return PartialView("_EditUser", model);

        }
        [HttpGet]
        public async Task<IActionResult> DeleteUser(string id)
        {
            string name = string.Empty;
            if (!String.IsNullOrEmpty(id))
            {
                ApplicationUser au = await this.userManager.FindByIdAsync(id);
                if (au != null)
                {
                    name = au.FirstName + "" + au.LastName;
                }
            }
            return PartialView("_DeleteUser", name);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteUser(string id, IFormCollection form)
        {
            if (!String.IsNullOrEmpty(id))
            {
                ApplicationUser au = await this.userManager.FindByIdAsync(id);
                if (au != null)
                {
                    IdentityResult auResult = await this.userManager.DeleteAsync(au);
                    if (auResult.Succeeded)
                    {
                        return RedirectToAction("Index");
                    }
                }
            }
            return View();
        }
    }
}