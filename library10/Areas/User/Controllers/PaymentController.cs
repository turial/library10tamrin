using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using library10.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace library10.Areas.User.Controllers
{
    [Area("User")]
    [Authorize(Roles = "User")]
    public class PaymentController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;

        public PaymentController(UserManager<ApplicationUser> userManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }
        [HttpGet]
        public IActionResult Payment()
        {
            var querfullname = (from u in _context.Users where u.Id == _userManager.GetUserId(HttpContext.User) select u).SingleOrDefault();
            ViewBag.kifepool = querfullname.KifePool;
            ViewBag.FullName = querfullname.FirstName + " " + querfullname.LastName;

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Payment(PaymentTransaction PT)
        {
            if (PT.Amount==0)
            {
                ModelState.AddModelError("amountError","مبلغ خالی می باشد لطفا مبلغی بیش از 100 تومان وارد نمایید.");
            }
            if (!ModelState.IsValid)
            {
                return View("Payment", PT);
            }
            var payment = await new ZarinpalSandbox.Payment(PT.Amount).PaymentRequest(PT.Description,
                Url.Action(nameof(PaymentVerify), "Payment", new

                {
                    Amount = PT.Amount,
                    Description = PT.Description,
                    Email = PT.Email,
                    Mobile = PT.Mobile,

                }, Request.Scheme), PT.Email, PT.Mobile);

            ///درصورت موفق بودن درخواست کاربر را به صفحه پرداخت هدایت میشود
            ///درغیر این صورت صفحه خطانمایش داده میشود

            return payment.Status == 100 ? (IActionResult)Redirect(payment.Link) :
                BadRequest($"کد خطا در پرداخت. کد خطا : {payment.Status}");
        }

        public async Task<IActionResult> PaymentVerify(int amount, string description, string email, string mobile, string Authority, string status)
        {
            var querfullname = (from u in _context.Users where u.Id == _userManager.GetUserId(HttpContext.User) select u).SingleOrDefault();
            ViewBag.kifepool = querfullname.KifePool;
            ViewBag.FullName = querfullname.FirstName + " " + querfullname.LastName;

            if (status == "Nok") return View("FailedPay");
            var verification = await new ZarinpalSandbox.Payment(amount).Verification(Authority);
            //اگر قبل از رسیدن به درگاه خطا دهد
            if (verification.Status != 100) return View("FailedPay");

            var RefId = verification.RefId;
            //بدست آوردن تاریخ تراکنش
            var currentDay = DateTime.Now;
            PersianCalendar pcCalendar = new PersianCalendar();
            int year = pcCalendar.GetYear(currentDay);
            int month = pcCalendar.GetMonth(currentDay);
            int day = pcCalendar.GetDayOfMonth(currentDay);
            string shamsiDate = string.Format("{0:yyyy/MM/dd}", Convert.ToDateTime(year + "/" + month + "/" + day));

            //بدست آوردن ساعت
            string GetTime = string.Format("{0:HH:mm:ss}",
            Convert.ToDateTime(currentDay.TimeOfDay.Hours + ":" + currentDay.TimeOfDay.Minutes + ":"+currentDay.TimeOfDay.Seconds));

            using (var database=_context)
            {
                using (var transaction=database.Database.BeginTransaction())
                {
                    try
                    {
                        PaymentTransaction P = new PaymentTransaction();
                        P.TransactionDate = shamsiDate;
                        P.TransactionTime = GetTime;
                        P.Amount = amount;
                        P.Description = description;
                        P.Mobile = mobile;
                        P.Email = email;
                        P.TransactionNumber = verification.RefId.ToString();
                        P.UserId = _userManager.GetUserId(User);
                        database.Add(P);
                        var updateQuery = (from U in database.Users where U.Id == _userManager.GetUserId(User) select U)
                            .SingleOrDefault();
                        updateQuery.KifePool += amount;
                        database.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }
            }
           
            ViewBag.TransactionNumber = verification.RefId;
            ViewBag.TransactionAmount = amount;
            ViewBag.TransactionDate = shamsiDate;
            ViewBag.TransactionTime = GetTime;
           
            return View("SuccessFullyPayment");

        }
    }
}