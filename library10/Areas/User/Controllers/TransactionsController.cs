using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using library10.Models;
using library10.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReflectionIT.Mvc.Paging;

namespace library10.Areas.User.Controllers
{
    [Area("User")]
    [Authorize(Roles = "User")]
    public class TransactionsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IServiceProvider _iServiceProvider;
        private readonly UserManager<ApplicationUser> _userManager;

        public TransactionsController(ApplicationDbContext context, IServiceProvider iServiceProvider, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _iServiceProvider = iServiceProvider;
            _userManager = userManager;
        }
        public async Task<IActionResult> Index(int page = 1)
        {
            //List<ManageRequestViewModel> model = new List<ManageRequestViewModel>();
            var model = (from pt in _context.PaymentTransactions
                         join u in _context.Users on pt.UserId equals u.Id
                         where u.Id == _userManager.GetUserId(User)
                         select new PaymentTransactionViewModel
                         {
                             Id = pt.Id,
                             Amount = pt.Amount,
                             Description = pt.Description,
                             Email = pt.Email,
                             Mobile = pt.Mobile,
                             TransactionDate = pt.TransactionDate,
                             TransactionTime = pt.TransactionTime,
                             TransactionNumber = pt.TransactionNumber,
                             UserId = pt.UserId
                         }).AsNoTracking().OrderBy(r => r.Id);
            var modelpaging = await PagingList<PaymentTransactionViewModel>.CreateAsync(model, 10, page);
            var querfullname = (from u in _context.Users where u.Id == _userManager.GetUserId(HttpContext.User) select u).SingleOrDefault();
            ViewBag.kifepool = querfullname.KifePool;
            ViewBag.FullName = querfullname.FirstName + " " + querfullname.LastName;

            return View(modelpaging);
        }

        public async Task<IActionResult> SearchInTransactions(string fromDate1, string todate1, string userSearch, int page = 1)
        {
            //List<ManageRequestViewModel> model = new List<ManageRequestViewModel>();
            var model = (from pt in _context.PaymentTransactions
                         join u in _context.Users on pt.UserId equals u.Id
                         where u.Id == _userManager.GetUserId(User)
                         select new PaymentTransactionViewModel
                         {
                             Id = pt.Id,
                             Amount = pt.Amount,
                             Description = pt.Description,
                             Email = pt.Email,
                             Mobile = pt.Mobile,
                             TransactionDate = pt.TransactionDate,
                             TransactionTime = pt.TransactionTime,
                             TransactionNumber = pt.TransactionNumber,
                             UserId = pt.UserId
                         }).AsNoTracking().OrderBy(r => r.Id);
            var modelpaging = await PagingList<PaymentTransactionViewModel>.CreateAsync(model, 10, page);


            if (userSearch != null)
            {
                modelpaging = await PagingList<PaymentTransactionViewModel>.CreateAsync(
                    model.Where(
                        r => r.UserFullName.Contains(userSearch)).OrderBy(r => r.Id), 10, page);
            }
            if (fromDate1 != null && todate1 == null)
            {
                //ازتاریخ فرام دیت یک به بعد
                modelpaging = await PagingList<PaymentTransactionViewModel>.CreateAsync(model.Where(
                    t => t.TransactionDate.CompareTo(fromDate1) >= 0).OrderBy(t => t.Id), 10, page);
            }

            if (todate1 != null && fromDate1 == null)
            {
                //ازتاریخ تودیت یک به قبل
                modelpaging = await PagingList<PaymentTransactionViewModel>.CreateAsync(model.Where(
                    t => t.TransactionDate.CompareTo(todate1) <= 0).OrderBy(t => t.Id), 10, page);
            }

            if (todate1 != null && fromDate1 != null)
            {
                modelpaging = await PagingList<PaymentTransactionViewModel>.CreateAsync(model.Where(
                    t => t.TransactionDate.CompareTo(todate1) <= 0 && t.TransactionDate.CompareTo(todate1) <= 0).OrderBy(t => t.Id), 10, page);

            }
            var querfullname = (from u in _context.Users where u.Id == _userManager.GetUserId(HttpContext.User) select u).SingleOrDefault();
            ViewBag.kifepool = querfullname.KifePool;
            ViewBag.FullName = querfullname.FirstName + " " + querfullname.LastName;

            return View("Index", modelpaging);
        }
    }
}