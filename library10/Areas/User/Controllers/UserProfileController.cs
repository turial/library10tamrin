using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using library10.Models;
using library10.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReflectionIT.Mvc.Paging;

namespace library10.Areas.User.Controllers
{
    [Area("User")]
    [Authorize(Roles = "User")]

    public class UserProfileController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IServiceProvider _iServiceProvider;
        private readonly UserManager<ApplicationUser> _userManager;

        private readonly IHostingEnvironment _appEnvironment;

        public UserProfileController(ApplicationDbContext context, IServiceProvider iServiceProvider, UserManager<ApplicationUser> userManager, IHostingEnvironment appEnvironment)
        {
            _context = context;
            _iServiceProvider = iServiceProvider;
            _userManager = userManager;
            _appEnvironment = appEnvironment;
        }
        public async Task<IActionResult> Index(int page=1)
        {

          //  List<ManageRequestViewModel> model = new List<ManageRequestViewModel>();
          var model = (from br in _context.BorrowBooks
                join b in _context.Books on br.BookId equals b.BookId
                join u in _context.Users on br.UserId equals u.Id
                //برای اینکه کتاب های مربوط به کاربری که لاگین کرده رو بیاره نه همه کتاب ها
                //با این شرط ای دی کاربری که لاگین کرده را میگریم ومقایسه میکنیم
                where u.Id == _userManager.GetUserId(HttpContext.User)
                select new ManageRequestViewModel
                {
                    Id = br.Id,
                    BookId = br.BookId,
                    UserId = br.UserId,
                    UserFullName = u.FirstName + " " + u.LastName,
                    BookStock = b.BookStock,
                    BookName = b.BookName,
                    Flag = br.Flag,
                    RequestDate = br.RequestDate,
                    AnswerDate = br.AnswerDate,
                    Price = br.Price,
                    BackDate = br.BackDate,
                    FlagDescription = (
                        br.Flag == 1 ? "درخواست امانت" :
                        br.Flag == 2 ? "به امانت برده" :
                        br.Flag == 3 ? "رد درخواست" :
                        br.Flag == 4 ? "برگردانده" : "نامشخص"
                    )
                }).AsNoTracking().OrderBy(r=>r.Id);
          var modelpaging = await PagingList<ManageRequestViewModel>.CreateAsync(model, 10, page);
            var querfullname = (from u in _context.Users where u.Id == _userManager.GetUserId(HttpContext.User) select u).SingleOrDefault();
            ViewBag.kifepool = querfullname.KifePool;
            ViewBag.FullName =querfullname.FirstName + " " + querfullname.LastName;
            return View(modelpaging);
        }
        public async Task<IActionResult> SearchInRequests(string fromDate1,string todate1,string bookSearch,int page=1)
        {
            //  List<ManageRequestViewModel> model = new List<ManageRequestViewModel>();
            var model = (from br in _context.BorrowBooks
                join b in _context.Books on br.BookId equals b.BookId
                join u in _context.Users on br.UserId equals u.Id
                //برای اینکه کتاب های مربوط به کاربری که لاگین کرده رو بیاره نه همه کتاب ها
                //با این شرط ای دی کاربری که لاگین کرده را میگریم ومقایسه میکنیم
                where u.Id == _userManager.GetUserId(HttpContext.User)
                select new ManageRequestViewModel
                {
                    Id = br.Id,
                    BookId = br.BookId,
                    UserId = br.UserId,
                    UserFullName = u.FirstName + " " + u.LastName,
                    BookStock = b.BookStock,
                    BookName = b.BookName,
                    Flag = br.Flag,
                    Price = br.Price,
                    RequestDate = br.RequestDate,
                    AnswerDate = br.AnswerDate,
                    BackDate = br.BackDate,
                    FlagDescription = (
                        br.Flag == 1 ? "درخواست امانت" :
                        br.Flag == 2 ? "به امانت برده" :
                        br.Flag == 3 ? "رد درخواست" :
                        br.Flag == 4 ? "برگردانده" : "نامشخص"
                    )
                }).AsNoTracking().OrderBy(r => r.Id);
            var modelpaging = await PagingList<ManageRequestViewModel>.CreateAsync(model, 10, page);


            if (bookSearch != null)
            {
                modelpaging = await PagingList<ManageRequestViewModel>.CreateAsync(
                    model.Where(
                        r => r.BookName.Contains(bookSearch)).OrderBy(r => r.Id), 10, page);
            }
            if (fromDate1 != null)
            {
                //ازتاریخ فرام دیت یک به بعد
                modelpaging = await PagingList<ManageRequestViewModel>.CreateAsync(model.Where(
                    t => t.RequestDate.CompareTo(fromDate1) >= 0).OrderBy(t => t.Id), 10, page);
            }

            if (todate1 != null)
            {
                //ازتاریخ تودیت یک به قبل
                modelpaging = await PagingList<ManageRequestViewModel>.CreateAsync(model.Where(
                    t => t.RequestDate.CompareTo(todate1) <= 0).OrderBy(t => t.Id), 10, page);
            }
            var querfullname = (from u in _context.Users where u.Id == _userManager.GetUserId(HttpContext.User) select u).SingleOrDefault();

            ViewBag.FullName = querfullname.FirstName + " " + querfullname.LastName;
            ViewBag.kifepool = querfullname.KifePool;
            return View("Index", modelpaging);
        }
        [HttpGet]
        public IActionResult ChangeUserPass()
        {
            var querfullname = (from u in _context.Users where u.Id == _userManager.GetUserId(HttpContext.User) select u).SingleOrDefault();
            ViewBag.kifepool = querfullname.KifePool;
            ViewBag.FullName = querfullname.FirstName + " " + querfullname.LastName;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ChangeUserPass(ChangeUserPassViewMole model)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = new ApplicationUser();
                user = (from u in _context.Users where u.Id == _userManager.GetUserId(HttpContext.User) select u).SingleOrDefault();
                if (await _userManager.CheckPasswordAsync(user,model.OldPassword))
                {
                    //correct oldpassword
                    await _userManager.ChangePasswordAsync(user,model.OldPassword,model.NewPassword);
                    ViewBag.changepass = "رمزعبور با موفقیت تغییر یافت.";
                    return View(model);
                }
                else
                {
                    //incorrect oldpassword
                    ModelState.AddModelError("OldPassword","رمزعبور قدیمی شما صحیح نمی باشد.");
                    return View(model);
                }
            }

            return View(model);
        }
    }
}