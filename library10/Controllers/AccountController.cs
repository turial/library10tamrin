using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using library10.Models;
using library10.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp;

namespace library10.Controllers
{
    public class AccountController : Controller
    {
        // از این کلاس برای لاگین کردن استفاده میشود
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public AccountController(SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }
        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, lockoutOnFailure: false);

                if (result.Succeeded)
                {
                    var user = await _userManager.FindByNameAsync(model.UserName);
                    var userRole = _userManager.GetRolesAsync(user).Result.Single();
                    //وقتی نام کاربری و رمز عبور صحیح باشد
                    return RedirectToLocal(returnUrl, userRole);
                }
                else
                {
                    // وقتی نام کاربری یارمز عبور صحیح نباشد
                    ModelState.AddModelError(string.Empty, "نام کاربری یا رمز عبور اشتباه است.");
                    return View(model);
                }
            }
            // وقتی نام کاربری یارمز عبور خالی باشد
            return View(model);

        }

        private IActionResult RedirectToLocal(string returnUrl, string RoleName)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                //اگر مسیر از قبل وجود داشت
                return Redirect(returnUrl);
            }
            else
            {
                //اگر مسیر اشتباه بود یا کار بر میخواهد لاگین کند
                if (RoleName == "Admin")
                {
                    return Redirect("/Admin/User");
                }
                else if (RoleName == "User")
                {
                    return Redirect("/User/UserProfile");

                }
            }

            return null;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOut()
        {
            if (Request.Cookies["_brbook"]!=null)
            {
                Response.Cookies.Delete("_brbook");
            }
            await _signInManager.SignOutAsync();
            return RedirectToAction("Login");
        }
    }
}