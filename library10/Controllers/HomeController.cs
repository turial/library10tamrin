﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using library10.Models;
using library10.Models.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;

namespace library10.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IServiceProvider _iServiceProvider;
        private readonly UserManager<ApplicationUser> _userManager;

        public HomeController(UserManager<ApplicationUser> userManager,ApplicationDbContext context, IServiceProvider iServiceProvider)
        {
            _context = context;
            _iServiceProvider = iServiceProvider;
            _userManager = userManager;
        }


        public IActionResult Index()
        {
            var  model = new MultiModels();
            model.LasBooks = (from b in _context.Books orderby b.BookId descending select b).Take(6).ToList();
            model.SientificBooks = (from b in _context.Books where b.BookgroupID == 1  orderby b.BookId descending select b).Take(6).ToList();
            model.Userlisted = (from u in _userManager.Users orderby u.Id descending select u).Take(10).ToList();
            model.LastNews = (from n in _context.news orderby n.NewsId descending select n).Take(5).ToList();
            model.MoreViewBook = (from b in _context.Books orderby b.BookViews descending select b).Take(6).ToList();
            ViewBag.imagepath = "/upload/normalImage/";
            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        public IActionResult Search(string textSearch)
        {
            var model = new MultiModels();
            model.Userlisted = (from u in _userManager.Users orderby u.Id descending select u).Take(10).ToList();
            model.LastNews = (from n in _context.news orderby n.NewsId descending select n).Take(5).ToList();
            //search query
            model.SearchedBooks = (from b in _context.Books where b.BookName.Contains(textSearch) orderby b.BookId descending select b).Take(10).ToList();
            ViewBag.imagepath = "/upload/normalImage/";
            ViewBag.word = textSearch;
            return View(model);
        }
    }
}
