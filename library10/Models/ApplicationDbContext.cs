﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;

namespace library10.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser,ApplicationRole,string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options):base(options)
        {
            
        }

        public DbSet<Author> Authors { get; set; }

        public DbSet<BookGroup> BookGroups { get; set; }

        public DbSet<Book> Books { get; set; }

       public DbSet<News>  news{ get; set; }
       public DbSet<BorrowBook> BorrowBooks { get; set; }
       public DbSet<PaymentTransaction> PaymentTransactions { get; set; }
    }
}
