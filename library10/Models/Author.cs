﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace library10.Models
{
    public class Author
    {
        //model baraye nevisandeha
        [Key]
        public int AuthorId  { get; set; }
        [Display(Name = "نام نویسنده")]
        [Required(ErrorMessage = "لطفا نام نویسنده را وارد نمایید.")]
        public string AuthorName { get; set; }
        [Display(Name = "توضیحات نویسنده")]
        [Required(ErrorMessage = "لطفا توضیحات را وارد نمایید.")]

        public string AuthorDiscription { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}
