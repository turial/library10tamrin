﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace library10.Models
{
    public class Book
    {
        //modele ketabha
        [Key]
        public int BookId { get; set; }

        public string BookName { get; set; }

        public string BookDescription { get; set; }

        public int BookPageCount { get; set; }

        public string BookImage { get; set; }

        public int BookStock { get; set; }

        public int Price { get; set; }

        public int BookViews { get; set; }

        public int BookLikeCount { get; set; }

        /////kelid khareji

        public int AuthorID { get; set; }
        [ForeignKey("AuthorID")]
        public virtual Author Authors { get; set; }



        public int BookgroupID { get; set; }
        [ForeignKey("BookgroupID")]
        public virtual BookGroup BookGroups { get; set; }

    }
}
