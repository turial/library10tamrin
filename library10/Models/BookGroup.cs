﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace library10.Models
{
    public class BookGroup
    {
        //modele groh bandi ketabha
        [Key]
        public int BookGroupId { get; set; }
        [Display(Name = "نام گروه")]
        [Required(ErrorMessage = "لطفا نام گروه بندی را وارد نمایید.")]

        public string BookGroupName { get; set; }
        [Display(Name = "توضیحات گروه")]
        [Required(ErrorMessage = "لطفا توضیحات گروه بندی را وارد نمایید.")]
        public string BookGroupDescription { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}
