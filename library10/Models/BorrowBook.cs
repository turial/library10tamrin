﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace library10.Models
{
    public class BorrowBook
    {
        [Key]
        public int Id { get; set; }

        public int BookId { get; set; }

        public int Price { get; set; }

        public string UserId { get; set; }

        public byte Flag { get; set; }

        public string RequestDate { get; set; }

        public string AnswerDate { get; set; }

        public string BackDate { get; set; }
    }
}
