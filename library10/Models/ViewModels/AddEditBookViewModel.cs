﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace library10.Models.ViewModels
{
    public class AddEditBookViewModel
    {
        [Key]
        public int BookId { get; set; }


        [Display(Name = "نام کتاب:")]
        [Required(ErrorMessage = "لطفا نام کتاب را وارد نمایید. ")]
        public string BookName { get; set; }
        

        [Display(Name = "توضیحات:")]
        [Required(ErrorMessage = "لطفا توضیحات را وارد نمایید. ")]
        public string BookDescription { get; set; }

        [Display(Name = "تعداد صفحات:")]
        public int BookPageCount { get; set; }

        [Display(Name = "مبلغ امانت")]
        [Required(ErrorMessage = "لطفا مبلغ امانت را وارد نمایید.")]
        public int Price { get; set; }

        [Display(Name = "تصویر کتاب:")]
       // [Required(ErrorMessage = "لطفا تصویر کتاب را وارد نمایید. ")]
        public string BookImage { get; set; }

        ////////dropdown list dar inja kelide khareji  nadarim vabe onvane drop down list estefade kardim
        [Display(Name = "گروه بندی:")]
        public int BookGroupId { get; set; }

        public List<SelectListItem> BookGroups { get; set; }

        [Display(Name = "نام نویسنده:")]
        public int AuthorId { get; set; }

        public List<SelectListItem> Authors { get; set; }


    }
}
