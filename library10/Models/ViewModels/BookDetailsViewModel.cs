﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace library10.Models.ViewModels
{
    public class BookDetailsViewModel
    {

        public int BookId { get; set; }

        [Display(Name = "نام کتاب")]
        public string BookName { get; set; }

        [Display(Name = "توضیحات کتاب")]
        public string BookDescription { get; set; }

        [Display(Name = "تعداد صفحات")]
        public int BookPageCount { get; set; }

        [Display(Name = "تصویر کتاب")]
        public string BookImage { get; set; }

        [Display(Name = "موجودی")]
        public int BookStock { get; set; }

        [Display(Name = "مبلغ امانت")]
        public int Price { get; set; }


        [Display(Name = "تعداد بازدید کتاب")]
        public int BookViews { get; set; }

        public int BookLikeCount { get; set; }

        public int AuthorId { get; set; }

        public int BookGroupId { get; set; }

        [Display(Name = "نام نویسنده")]
        public string AuthorName { get; set; }

        [Display(Name = "نام گروه بندی")]
        public string BookGroupName { get; set; }
    }
}
