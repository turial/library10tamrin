﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace library10.Models.ViewModels
{
    public class BookListViewModel
    {
        public int BookId { get; set; }
        [Display(Name = "نام کتاب")]
        public string BookName { get; set; }

        [Display(Name = "تعداد صحفات")]
        public int BookPageCount { get; set; }
        [Display(Name = "تصویر کتاب")]
        public string BookImage { get; set; }
        public int AuthorId { get; set; }
        public int BookGroupId { get; set; }
        [Display(Name = "نام نویسنده")]
        public string AuthorName { get; set; }
        [Display(Name = "نام گروه")]
        public string BookGroupName { get; set; }

        [Display(Name = "مبلغ امانت")]
        public int Price { get; set; }
    }
}
