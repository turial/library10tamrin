﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace library10.Models.ViewModels
{
    public class ChangeUserPassViewMole
    {
        public string Id { get; set; }



        [Display(Name = "رمز عبور قدیمی")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "لطفا رمز عبور قدیمی را وارد نمایید.")]
        [StringLength(10, MinimumLength = 6, ErrorMessage = "حداقل طول رمز عبور 6 کاراکتر و حداکثر طول رمز عبور 10 کاراکتر باید باشد.")]
        public string OldPassword { get; set; }

        [Display(Name = "رمز عبور جدید")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "لطفا رمز عبور جدید را وارد نمایید.")]
        [StringLength(10, MinimumLength = 6, ErrorMessage = "حداقل طول رمز عبور 6 کاراکتر و حداکثر طول رمز عبور 10 کاراکتر باید باشد.")]
        [RegularExpression("^((?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])|(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[^a-zA-Z0-9])|(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])|(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])).{3,}$", ErrorMessage = "رمزعبو باید ترکیبی از اعداد و حروف کوچک و بزرگ باشد.")]

        public string NewPassword { get; set; }

        [Display(Name = "تکرار رمز عبور")]
        [Required(ErrorMessage = "لطفا  تکرار رمز عبور جدید را وارد نمایید.")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "رمز عبور با تکرار آن یکسان نیست.")]
        public string ConfirmNewPassword { get; set; }




    }
}
