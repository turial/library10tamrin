﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace library10.Models.ViewModels
{
    public class EditUserViewModel
    {
        public string Id { get; set; }
        
        [Display(Name = "نام")]
        [Required(ErrorMessage = "لطفا نام را وارد نمایید.")]
        public string FirstName { get; set; }

        [Display(Name = "نام خانوادگی")]
        [Required(ErrorMessage = "لطفا نام خانوادگی را وارد کنید.")]
        public string LastName { get; set; }

        [Display(Name = "ایمیل")]
        [Required(ErrorMessage = "لطفا ایمیل را وارد نمایید.")]
        //[DataType(DataType.EmailAddress)]
        //[EmailAddress(ErrorMessage = "ایمیل معتبر نیست.")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "ایمیل نا معتبر است.")]
        public string Email { get; set; }

        [Display(Name = "جنسیت")]
        public byte Gender { get; set; }


        /// <summary>
        /// ///// drop down lis 
        /// </summary>

        public List<SelectListItem> ApplicationRoles { get; set; }

        [Display(Name = "نقش")]
        [Required(ErrorMessage = "لطفا نقش را وارد نمایید.")]
        public string ApplicationRoleId { get; set; }
    }
}
