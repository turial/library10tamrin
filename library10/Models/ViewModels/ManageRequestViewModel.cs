﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace library10.Models.ViewModels
{
    public class ManageRequestViewModel
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public int BookId { get; set; }

        public byte Flag { get; set; }

        [Display(Name = "وضعیت")]
        public string FlagDescription { get; set; }

        [Display(Name = "مبلغ امانت")]
        public int Price { get; set; }

        [Display(Name="نام کاربر")]
        public string UserFullName { get; set; }

        [Display(Name = "نام کتاب")]
        public string BookName { get; set; }

        [Display(Name = "موجودی کتاب")]
        public int BookStock { get; set; }

        [Display(Name = "تاریخ درخواست")]
        public string RequestDate { get; set; }

        [Display(Name = "تاریخ پاسخ")]
        public string AnswerDate { get; set; }

        [Display(Name = "بازگشت")]
        public string BackDate { get; set; }

    }
}
