﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace library10.Models.ViewModels
{
    public class MultiModels
    {
        public List<Book> LasBooks { get; set; }
        public List<Book> SientificBooks { get; set; }
        public List<Book> SearchedBooks { get; set; }
        public List<ApplicationUser> Userlisted { get; set; }
        public List<News> LastNews { get; set; }
        public List<BookDetailsViewModel> bookDetail { get; set; }
        public List<Book> MoreViewBook { get; set; }
    }
}
