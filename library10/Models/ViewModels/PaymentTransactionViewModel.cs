﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace library10.Models.ViewModels
{
    public class PaymentTransactionViewModel
    {
        [Key]
        public int Id { get; set; }


        [Display(Name = "تاریخ تراکنش")]
        public string TransactionDate { get; set; }

        [Display(Name = "زمان تراکنش")]
        public string TransactionTime { get; set; }

        [Display(Name = "مبلغ امانت")]
        public int Amount { get; set; }

        [Display(Name = "توضیحات")]
        public string Description { get; set; }


        [Display(Name = "ایمیل")]
        [Required(ErrorMessage = "لطفا ایمیل را وارد نمایید.")]
        public string Email { get; set; }

        [Display(Name = "شماره تماس")]
        [Required(ErrorMessage = "لطفا شماره تماس را وارد نمایید.")]
        public string Mobile { get; set; }

        [Display(Name = "شماره تراکنش")]
        public string TransactionNumber { get; set; }


        [Display(Name = "نام و نام خانوادگی ")]
        public string UserFullName { get; set; }

        public string UserId { get; set; }

        [ForeignKey("UserId")]

        public virtual ApplicationUser Users { get; set; }
    }
}
