﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
namespace library10.Models.ViewModels
{
    public class UserViewModel
    {
        public string Id { get; set; }


       [Display(Name = "نام کاربری")]
       [Required(ErrorMessage = "لطفا نام کاربری را وارد نمایید.")]
        public string UserName { get; set; }


        [Display(Name = "رمز عبور")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "لطفا رمز عبور را وارد نمایید.")]
        [StringLength(10,MinimumLength = 6,ErrorMessage = "حداقل طول رمز عبور 6 کاراکتر و حداکثر طول رمز عبور 10 کاراکتر باید باشد.")]
        public string Password { get; set; }


        [Display(Name = "تکرار رمز عبور")]
        [Required(ErrorMessage = "لطفا  تکرار رمز عبور را وارد نمایید.")]
        [DataType(DataType.Password)]
        [Compare("Password",ErrorMessage = "رمز عبور با تکرار آن یکسان نیست.")]
        public string ConfirmPassword { get; set; }


        //[Display(Name = "نام و نام خانوادگی")]
        //[Required(ErrorMessage = "لطفا  نام و نام خانوادگی را وارد نمایید.")]
        //public string FullName { get; set; }


        [Display(Name = "نام")]
        [Required(ErrorMessage = "لطفا نام را وارد نمایید.")]
        public string FirstName { get; set; }

        [Display(Name = "نام خانوادگی")]
        [Required(ErrorMessage = "لطفا خانوادگی را وارد نمایید.")]
        public string LastName { get; set; }

        [Display(Name = "شماره تلفن")]
        [Required(ErrorMessage = "لطفا شماره تلفن را وارد نمایید.")]
        public string PhoneNumber { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "ایمیل")]
        [Required(ErrorMessage = "لطفا ایمیل را وارد نمایید.")]
        public string Email { get; set; }
        [Display(Name = "جنسیت")]
        public byte Gender { get; set; }


        /// <summary>
        /// ///// drop down lis 
        /// </summary>

        public List<SelectListItem> ApplicationRoles { get; set; }
 
        [Display(Name = "نقش")]
        [Required(ErrorMessage = "لطفا نقش را وارد نمایید.")]
        public string ApplicationRoleId { get; set; }
    }
}
