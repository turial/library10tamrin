﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace library10.PublicModel
{
    public class ModalHeader
    {
        public string Heading { get; set; }
    }
}
