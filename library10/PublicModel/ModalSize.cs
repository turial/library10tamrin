﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace library10.PublicModel
{
    public enum ModalSize
    {
        Small,
        Large,
        Medium
    }
}
