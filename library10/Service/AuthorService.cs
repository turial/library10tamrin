﻿using library10.Models;
using Microsoft.EntityFrameworkCore;
using ReflectionIT.Mvc.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace library10.Service
{

    public class AuthorService : IAuthorService
    {
        private readonly ApplicationDbContext _db;

        public AuthorService(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<List<Author>> AuthorListAsync()
        {
            var model = _db.Authors.AsNoTracking().Include(a => a.Books).OrderBy(a => a.AuthorId);
            var modelpaging = await PagingList<Author>.CreateAsync(model, 10, 1);
            return modelpaging;
        }

        public async Task<List<Author>> SearchAuthorAsync(string authorSearch)
        {
            var model = _db.Authors.AsNoTracking().Include(a => a.Books).OrderBy(a => a.AuthorId);
            var modelpaging = await PagingList<Author>.CreateAsync(model, 10, 1);
            if (authorSearch != null)
            {
                modelpaging = await PagingList<Author>.CreateAsync(model.Where(
                    a => a.AuthorName.Contains(authorSearch)).OrderBy(a => a.AuthorId), 10, 1);
            }
            return modelpaging;
        }

        public async Task<Author> GetAuthorByIdAsync(int Id)
        {
            Author author = new Author();
            author = await _db.Authors.Where(a => a.AuthorId == Id).SingleOrDefaultAsync();
            return author;
        }
        //virayesh or update
        public void EditAuthor(Author author)
        {
            _db.Entry(author).State = EntityState.Modified;
        }

        public void InsertAuthor(Author author)
        {
            _db.Authors.Add(author);
        }

        public void DeleteAuthor(Author author)
        {
            _db.Entry(author).State = EntityState.Deleted;
        }

        public void Save()
        {
            _db.SaveChanges();
        }
    }
}
