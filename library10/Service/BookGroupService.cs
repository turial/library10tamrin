﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using library10.Models;
using Microsoft.EntityFrameworkCore;
using ReflectionIT.Mvc.Paging;

namespace library10.Service
{
    public class BookGroupService : IBookGroupService
    {
        private readonly ApplicationDbContext _db;

        public BookGroupService(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<List<BookGroup>> PagingListBookGroupAsync()
        {
            var model = _db.BookGroups.AsNoTracking().Include(b => b.Books).OrderBy(bg => bg.BookGroupId);
            var modelpaging = await PagingList<BookGroup>.CreateAsync(model, 10, 1);
            return modelpaging;

        }

        public async Task<List<BookGroup>> SearchBookGroupAsync(string groupSearch)
        {
            var model = _db.BookGroups.AsNoTracking().Include(b => b.Books).OrderBy(bg => bg.BookGroupId);
            var modelpaging = await PagingList<BookGroup>.CreateAsync(model, 10, 1);
            if (groupSearch != null)
            {
                modelpaging = await PagingList<BookGroup>.CreateAsync(model.Where(bg => bg.BookGroupName.Contains(groupSearch)).OrderBy(bg => bg.BookGroupId), 10, 1);
            }

            return modelpaging;

        }

        public async Task<BookGroup> GetBookGroupByIdAsync(int Id)
        {
            BookGroup bookGroup = new BookGroup();
            bookGroup = await _db.BookGroups.Where(bg => bg.BookGroupId == Id).SingleOrDefaultAsync();
            return bookGroup;
        }

        public void EditBookGroup(BookGroup bookGroup)
        {
            _db.Entry(bookGroup).State = EntityState.Modified;
        }

        public void InsertBookGroup(BookGroup bookGroup)
        {
            _db.BookGroups.Add(bookGroup);
        }

        public void DeleteBookGroup(BookGroup bookGroup)
        {
            _db.Entry(bookGroup).State = EntityState.Deleted;
        }

        public void Save()
        {
            _db.SaveChanges();
        }
    }
}
