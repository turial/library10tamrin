﻿using library10.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace library10.Service
{
    public interface IAuthorService
    {
        Task<List<Author>> AuthorListAsync();
        Task<List<Author>> SearchAuthorAsync(string authorSearch);
        Task<Author> GetAuthorByIdAsync(int Id);
        void EditAuthor(Author author);
        void InsertAuthor(Author author);
        void DeleteAuthor(Author author);
        void Save();
    }
}
