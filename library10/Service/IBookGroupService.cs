﻿using library10.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace library10.Service
{
    public interface IBookGroupService
    {
        Task<List<BookGroup>> PagingListBookGroupAsync();
        Task<List<BookGroup>> SearchBookGroupAsync(string groupSearch);
        Task<BookGroup> GetBookGroupByIdAsync(int Id);
        void EditBookGroup(BookGroup bookGroup);
        void InsertBookGroup(BookGroup bookGroup);
        void DeleteBookGroup(BookGroup bookGroup);
        void Save();
    }
}
