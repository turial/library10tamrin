﻿(function ($) {
    function ManageRequest() {
        var $this = this;

        function initilizeModel() {
            $("#modal-action-request").on('loaded.bs.modal', function (e) {

            }).on('hidden.bs.modal', function (e) {
                $(this).removeData('bs.modal');
            });
        }
        $this.init = function () {
            initilizeModel();
        }
    }
    $(function () {
        var self = new ManageRequest();
        self.init();
    })
}(jQuery))